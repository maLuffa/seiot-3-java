package seiot.model.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import seiot.model.core.Observer;
/**
 * all logObserver, a factory
 */
public interface LogObserver {
	static Observer consoleObserver() {
		return (e) -> {
			if(e instanceof LogEvent) {
				System.out.println(((LogEvent) e).getLog());
			}
			return true;
		};
	}
	static Observer fileObserver(final String file) throws FileNotFoundException {
		final PrintStream ps = new PrintStream(new File(file));
		return (e) -> {
			if(e instanceof LogEvent) {
				ps.println(((LogEvent) e).getLog());
			}
			return true;
		};
	}
	
}
