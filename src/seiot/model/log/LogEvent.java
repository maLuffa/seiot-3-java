package seiot.model.log;

import seiot.model.core.Event;
/**
 * an event produced by a logger
 */
public class LogEvent implements Event{
    private final String log;
    
    public LogEvent(final String log) {
        this.log = log;
    }
    
    public String getLog(){
        return this.log;
    }
}
