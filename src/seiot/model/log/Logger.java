package seiot.model.log;

import seiot.model.core.Observable;
/**
 * a source of log
 */
public class Logger extends Observable{
    private static final Logger SINGLETON = new Logger();
    public static Logger instance() {
        return SINGLETON;
    }
    public void log(final String string) {
        super.notifyEvent(new LogEvent(string));
    }
}
