package seiot.model.core;

import java.util.LinkedList;
/**
 * a source of event
 */
public class Observable {
	
	private LinkedList<Observer> observers;
	
	protected Observable(){
		observers = new LinkedList<Observer>();
	}
	
	protected void notifyEvent(Event ev){
		synchronized (observers){
			for (Observer obs: observers){
				obs.notifyEvent(ev);
			}
		}
	}
	/**
	 * a method called to attach an observer on this source
	 * @param obs
	 * 	the observer
	 */
	public void addObserver(Observer obs){
		synchronized (observers){
			observers.add(obs);
		}
	}
	/**
	 * remove an observer to this source
	 * @param obs
	 * 	the observer
	 */
	public void removeObserver(Observer obs){
		synchronized (observers){
			observers.remove(obs);
		}
	}

}
