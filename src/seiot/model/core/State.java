package seiot.model.core;
/**
 * root interface of state used into state pattern
 */
public interface State {
	/**
	 * exec current step
	 */
    void exec();
    /**
     * 
     * @return
     * 	next state of the FSM machine
     */
    State nextState();
}
