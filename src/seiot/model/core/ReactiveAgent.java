package seiot.model.core;

import seiot.model.msg.Msg;
import seiot.model.msg.MsgEvent;
/**
 * basic agent
 */
public abstract class ReactiveAgent extends BasicEventLoopController {
	
	protected ReactiveAgent(int size){
		super(size);
	}

	protected ReactiveAgent(){
	}
	
	protected boolean sendMsgTo(ReactiveAgent agent, Msg m){
		MsgEvent ev = new MsgEvent(m,this);
		return agent.notifyEvent(ev);
	}
}
