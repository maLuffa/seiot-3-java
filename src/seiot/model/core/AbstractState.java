package seiot.model.core;
/**
 * the root of basic state
 */
public abstract class AbstractState implements State {
    protected State _nextState;
    public AbstractState(){
        this._nextState = this;
    }
    public State nextState() {
        return this._nextState;
    }
}
