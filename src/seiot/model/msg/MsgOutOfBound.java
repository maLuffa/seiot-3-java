package seiot.model.msg;
/**
 * a message to say that something is out of bound
 */
public class MsgOutOfBound implements Msg{
	private final double angle;
	
	public MsgOutOfBound(final double angle) {
		this.angle = angle;
	}
	
	public double getAngle() {
		return this.angle;
	}

}
