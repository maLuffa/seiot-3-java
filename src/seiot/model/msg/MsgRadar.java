package seiot.model.msg;
/**
 * a message produce by a radar
 */
public class MsgRadar implements Msg{ 
	private final Double angle;
	private final Double distance;
	
	public MsgRadar(double distance, Double angle) { 
		this.angle = angle;
		this.distance = distance;
	}
	
	public Double getDistance() {
		return this.distance;
	}
	
	public Double getAngle() {
		return this.angle;
	}
}
