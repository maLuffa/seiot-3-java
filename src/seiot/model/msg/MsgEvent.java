package seiot.model.msg;

import seiot.model.core.Event;
import seiot.model.core.ReactiveAgent;
/**
 * an event produced by an agent with a message
 */
public class MsgEvent implements Event {
	
	private Msg msg;
	private ReactiveAgent from;
	
	public MsgEvent(Msg msg, ReactiveAgent from){
		this.msg = msg;
		this.from = from;
	}

	public MsgEvent(Msg msg){
		this.msg = msg;
	}
	
	public Msg getMsg(){
		return msg;
	}
	
	public ReactiveAgent getFrom(){
		return from;
	}
}
