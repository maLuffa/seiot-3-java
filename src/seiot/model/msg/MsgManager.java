package seiot.model.msg;
/**
 * all messages produce by a manager
 */
public interface MsgManager {
	public static final class TurnOn implements Msg{}
	
	public static final class TurnOff implements Msg{}
	
	public static final class Stop implements Msg{}
	
	public static final class Restart implements Msg{}
		
}
