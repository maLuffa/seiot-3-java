package seiot.model.event;

import seiot.model.core.Event;
/**
 * an event verify when an angle change
 */
public class AngleEvent implements Event{
	private final double angle;
	
	public AngleEvent(final double angle) {
		this.angle = angle;
	}
	/**
	 * 
	 * @return
	 * 	the current angle
	 */
	public double getAngle() {
		return this.angle;
	}
}

