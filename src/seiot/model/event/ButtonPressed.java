package seiot.model.event;

import seiot.model.core.*;
import seiot.model.device.Button;
/**
 * an event produced when a buttun is pressed
 */
public class ButtonPressed implements Event {
	private Button source;
	
	public ButtonPressed(Button source){
		this.source = source;
	}
	
	public Button getSourceButton(){
		return source;
	}
}
