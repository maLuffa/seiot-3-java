package seiot.model.event;

import seiot.model.core.Event;
/**
 * an event produced when a distance change
 */
public class DistanceEvent implements Event{
	private final double distance;
	
	public DistanceEvent(final double distance) {
		this.distance = distance;
	}
	/**
	 * 
	 * @return
	 * 	the current distance
	 */
	public double getDistance() {
		return this.distance;
	}
}
