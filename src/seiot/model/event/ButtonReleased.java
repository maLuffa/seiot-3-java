package seiot.model.event;

import seiot.model.core.*;
import seiot.model.device.Button;
/**
 * an avent produced when a button is released
 */
public class ButtonReleased implements Event {
	private Button source;
	
	public ButtonReleased(Button source){
		this.source = source;
	}
	
	public Button getSourceButton(){
		return source;
	}
}
