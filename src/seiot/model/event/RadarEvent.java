package seiot.model.event;

import seiot.model.core.Event;
/**
 * an event produced by a radar
 */
public class RadarEvent implements Event{
    private final float distance;
    private final float angle;
    
    public RadarEvent(final float distance,final float angle) {
        this.distance = distance;
        this.angle = angle;
    }
    
    public float getAngle(){
        return this.angle;
    }
    
    public float getDistance(){
        return this.distance;
    }
}
