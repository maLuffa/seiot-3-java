package seiot.model.event;

import seiot.model.core.*;
/**
 * an event produced by a clock
 */
public class Tick implements Event {
	
	private long time;
	
	public Tick(long time ){
		this.time = time;
	}
	
	public long getTime(){
		return time;
	}
}
