package seiot.model.device.emu;

import seiot.model.core.Event;
import seiot.model.device.ObservableRadar;
import seiot.model.event.AngleEvent;
import seiot.model.event.DistanceEvent;

public class ObservableRadarImpl extends ObservableRadar {
	private static final int TO_CM = 300;
	private static final int MIN_ANGLE = 0;
	private static final int MAX_ANGLE = 180;
	private static int DELTA = 5;
	private boolean on;
	private boolean go;
	private double angle;
	private Event lastDistance;
	private Event lastAngle;
	public ObservableRadarImpl() {
		this.on = false;
		this.go = false;
		new Emu().start();
		angle = 0;
	}
	@Override
	public void stop() {
		this.go = false;
	}

	@Override
	public void restart() {
		this.go = true;
	}

	@Override
	public void on() {
		this.on = true;
		this.go = true;
	}

	@Override
	public void off() {
		this.on = false;
		this.go = false;
	}
	
	private class Emu extends Thread{
		
		public void run() {
			while(true) {
				if(on) {
					lastDistance = new DistanceEvent(Math.random() * TO_CM);
					if(go) {
						angle += DELTA;
						lastAngle = new AngleEvent(angle);
						if(angle == MIN_ANGLE || angle == MAX_ANGLE) {
							DELTA = -DELTA;
						}
					}
					ObservableRadarImpl.this.notifyEvent(lastDistance);
					ObservableRadarImpl.this.notifyEvent(lastAngle);
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public double getMaxDist() {
		return 200;
	}
	@Override
	public void increaseVelocity() {}
	@Override
	public void decreaseVelocity() {}
}
