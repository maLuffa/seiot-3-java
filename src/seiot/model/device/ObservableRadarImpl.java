package seiot.model.device;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import seiot.model.event.AngleEvent;
import seiot.model.event.DistanceEvent;
/**
 * the implementation of radar
 */
public class ObservableRadarImpl extends ObservableRadar implements SerialPortEventListener{
	private static final int BOUND_RATE = 9600;
	private final BufferedReader input;
	private final OutputStream output;
	private final double maxDist;
	private boolean isDistance;
	public ObservableRadarImpl(final String port, double maxDist) throws Exception {
		CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(port);
		SerialPort serialPort = (SerialPort) portId.open(this.getClass().getName(), 2000);
		this.maxDist = maxDist;
		serialPort.setSerialPortParams(BOUND_RATE,
				SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1,
				SerialPort.PARITY_NONE);

		input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
		output = serialPort.getOutputStream();
		this.isDistance = true;
		serialPort.addEventListener(this);
		serialPort.notifyOnDataAvailable(true);
	}
	@Override
	public void stop() {
		sendMsg(Command.STOP);
	}
	@Override
	public void restart() {
		sendMsg(Command.RESTART);	
	}
	@Override
	public void on() {
		sendMsg(Command.ON);
	}
	@Override
	public void off() {
		sendMsg(Command.OFF);	
	}
	@Override
	public void increaseVelocity() {
		sendMsg(Command.PLUS);
	}
	@Override
	public void decreaseVelocity() {
		sendMsg(Command.MINUS);
	}
	@Override
	public double getMaxDist() {
		return this.maxDist;
	}
	private void sendMsg(Command cmd) {
		try {
			output.write(cmd.getMsg());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private static enum Command{
		STOP('2'),
		RESTART('3'),
		ON('1'),
		OFF('4'),
		PLUS('5'),
		MINUS('6');
		
		private final char msg;
		
		Command(char msg) {
			this.msg = msg;
		}
		
		char getMsg() {
			return this.msg;
		}
	}

	@Override
	public void serialEvent(SerialPortEvent event) {
		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				double msg= Double.parseDouble(input.readLine());
				if(this.isDistance) {
					this.notifyEvent(new DistanceEvent(msg));
				} else {
					this.notifyEvent(new AngleEvent(msg));
				}
				this.isDistance = !this.isDistance;
			} catch (Exception e) {
			}
		}
	}

}
