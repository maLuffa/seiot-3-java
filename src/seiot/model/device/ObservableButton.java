package seiot.model.device;

import seiot.model.core.Observable;
/**
 * a source off button event
 */
public abstract class ObservableButton extends Observable implements Button {

}
