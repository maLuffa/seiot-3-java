package seiot.model.device;
/**
 * root interface of button
 */
public interface Button {
	/**
	 * 
	 * @return
	 * 	true if is pressed false if isn't pressed
	 */
	boolean isPressed();

}
