package seiot.model.device;

import java.io.IOException;
/**
 * a root interface of a light
 */
public interface Light {
	/**
	 * switch on the light
	 */
	void switchOn() throws IOException;
	/**
	 * switch off the light
	 */
	void switchOff() throws IOException;
}
