package seiot.model.device;

import seiot.model.core.Observable;
/**
 * a source of radar events
 */
public abstract class ObservableRadar extends Observable implements Radar { }
