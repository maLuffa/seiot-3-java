package seiot.model.device;
/**
 * an interface of generic radar
 */
public interface Radar {
    /**
     * Stop the radar
     */
    void stop();
    /**
     * Restart the radar
     */
    void restart();
    /**
     * Switch on the radar
     */
    void on();
    /**
     * Switch off the radar
     */
    void off();
    /**
     * increase the velocity of radar
     */
    void increaseVelocity();
    /**
     * decrease the velocity of radar
     */
    void decreaseVelocity();
    /**
     * 
     * @return
     * 	the max distance checkable
     */
    double getMaxDist();
}
