package seiot.model.test;

import java.util.Observable;

import seiot.model.core.Event;
import seiot.model.core.Observer;
import seiot.model.device.ObservableRadar;
import seiot.model.device.emu.ObservableRadarImpl;
import seiot.model.event.AngleEvent;
import seiot.model.event.DistanceEvent;

public class TestObservableRadar {
	public static void main(String[] args) throws InterruptedException {
		ObservableRadar ob = new ObservableRadarImpl();
		new RadarObserver(ob);
		int i = 0;
		while(true) {
			switch(i) {
			case 0:
				ob.on();
			break;
			case 1:
				ob.stop();
			break;
			case 2:
				ob.restart();
			break;
			case 3:
				ob.off();
			break;
			}
			i=(i+1)%4;
			Thread.sleep(2000);
		}
	}
	private static class RadarObserver implements Observer {
		
		public RadarObserver(final ObservableRadar radar) {
			radar.addObserver(RadarObserver.this);
		}
		@Override
		public boolean notifyEvent(Event ev) {
			if(ev instanceof DistanceEvent) {
				System.out.println("DISTANCE : " + (((DistanceEvent) ev).getDistance()));
				return true;
			} else if(ev instanceof AngleEvent) {
				System.out.println("ANGLE : " + ((AngleEvent)ev).getAngle());
				return true;
			}
			return false;
		}
		
	}
}
