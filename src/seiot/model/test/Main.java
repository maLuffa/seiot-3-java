package seiot.model.test;
/**
 * MODE :
 * DEBUG-> command yes : all thing are emulated
 * SEMI -> command semi : only arduino is real
 * NO DEBUG -> command no: all is real
 */
import java.io.FileNotFoundException;

import seiot.model.agent.Blinker;
import seiot.model.agent.ButtonAgent;
import seiot.model.agent.Manager;
import seiot.model.agent.RadarAgent;
import seiot.model.device.Light;
import seiot.model.device.ObservableButton;
import seiot.model.device.ObservableTimer;
import seiot.model.log.LogObserver;
import seiot.model.log.Logger;
import seiot.model.msg.MinMsg;
import seiot.model.msg.MsgButton;
import seiot.model.msg.PlusMsg;
public class Main {
	private static final int MAX_DIST = 200;

	public static void main(String[] args) {
		if(args.length != 2) {
			System.out.println("NOT ENOUGH PARAMETERS!");
			System.exit(-1);
		}
		ObservableButton onBtn = null;
		ObservableButton offBtn = null;
		ObservableButton plusBtn = null;
		ObservableButton minBtn = null;
		Light tracking = null;
		Light on = null;
		Light detected = null;
		RadarAgent radar = null;
		Manager manager = null;
		Blinker blinking = null;
		if(args[1].equals("yes") || args[1].equals("semi") ) {
			//DEBUG MODE : ON
			onBtn = new seiot.model.device.emu.ObservableButton(10);
			offBtn = new seiot.model.device.emu.ObservableButton(11);
			plusBtn = new seiot.model.device.emu.ObservableButton(12);
			minBtn = new seiot.model.device.emu.ObservableButton(13);
			tracking = new seiot.model.device.emu.Led(1,"TRACKING");
			on = new seiot.model.device.emu.Led(2,"ON");
			detected = new seiot.model.device.emu.Led(3,"DETECTED");
			blinking = new Blinker(detected, new ObservableTimer());
			manager = new Manager(on,blinking, tracking);
			try {

		                if(!args[1].equals("semi")) {
		                    radar = new RadarAgent(manager, new seiot.model.device.emu.ObservableRadarImpl());
		                } else {
		                    radar = new RadarAgent(manager, new seiot.model.device.ObservableRadarImpl(args[0],MAX_DIST)); 
		                }
		                manager.setDestination(radar);
				radar.start();
			} catch (Exception e1) {
				System.err.println("ARDUINO DOESN'T WORK");
				System.exit(-1);
			}
		} else {
			//DEBUG MODE : OFF
			onBtn = new seiot.model.device.raspi.Button(15);
			offBtn = new seiot.model.device.raspi.Button(16);
			plusBtn = new seiot.model.device.emu.ObservableButton(12);
			minBtn = new seiot.model.device.emu.ObservableButton(12);
			tracking = new seiot.model.device.raspi.Led(8);
			on = new seiot.model.device.raspi.Led(7);
			detected = new seiot.model.device.raspi.Led(9);
			blinking = new Blinker(detected, new ObservableTimer());
			manager = new Manager(on,blinking, tracking);
			try {
				tracking.switchOff();
				on.switchOff();
				detected.switchOff();
				radar = new RadarAgent(manager, new seiot.model.device.ObservableRadarImpl(args[0],MAX_DIST));
				manager.setDestination(radar);
				radar.start();
			} catch (Exception e1) {
				System.err.println("ARDUINO DOESN'T WORK");
				System.exit(-1);
			}
		}

		ButtonAgent onAgent = new ButtonAgent(new MsgButton.On(), manager, onBtn);
		ButtonAgent offAgent = new ButtonAgent(new MsgButton.Off(), manager, offBtn);
		ButtonAgent plusAgent = new ButtonAgent(new PlusMsg(), radar, plusBtn);
		ButtonAgent minusAgent = new ButtonAgent(new MinMsg(), radar, minBtn);
		manager.start();
		onAgent.start();
		offAgent.start();
		blinking.start();
		plusAgent.start();
		minusAgent.start();
		Logger.instance().addObserver(LogObserver.consoleObserver());
		try {
			Logger.instance().addObserver(LogObserver.fileObserver("log.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
