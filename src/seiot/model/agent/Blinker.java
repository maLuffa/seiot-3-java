package seiot.model.agent;

import java.io.IOException;

import seiot.model.core.AbstractState;
import seiot.model.core.BasicEventLoopController;
import seiot.model.core.Event;
import seiot.model.device.Light;
import seiot.model.device.ObservableTimer;
import seiot.model.event.Tick;
import seiot.model.event.TimerDelta;
import seiot.model.log.Logger;
/**
 * An agent used to blink a led
 */
public class Blinker extends BasicEventLoopController {
    private static final String ERR = "NO LED!!!!";
    private Light led;
    private ObservableTimer timer;
    private seiot.model.core.State currentState;
    private Event ev;
    public Blinker(Light led,ObservableTimer timer){
        this.led = led;
        this.timer = new ObservableTimer();     
        this.startObserving(timer);
        this.timer = timer;
        this.currentState = new Off();
    }
    private final class Off extends AbstractState {

        @Override
        public void exec() {
            if(ev instanceof TimerDelta) {
                try {
                    led.switchOn();
                } catch (IOException e) {
                    Logger.instance().log(ERR);
                }
                timer.start(((TimerDelta)ev).getDelta());
                currentState = new On();
            }
        }
        
    }
    private final class On extends AbstractState {
        @Override
        public void exec() {
            if(ev instanceof Tick) {
                try {
                    led.switchOff();
                } catch (IOException e) {
                    Logger.instance().log(ERR);
                }
                currentState = new Off();
                timer.stop();
            }
        }
    }
    protected void processEvent(Event ev){
        this.ev = ev;
        this.currentState.exec();
        this.currentState = this.currentState.nextState();
    }
}
