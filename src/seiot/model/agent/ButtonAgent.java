package seiot.model.agent;

import seiot.model.core.Event;
import seiot.model.core.Observer;
import seiot.model.core.ReactiveAgent;
import seiot.model.device.ObservableButton;
import seiot.model.event.ButtonPressed;
import seiot.model.msg.Msg;
/**
 * An agent used to observe a button an send message to another agent
 */
public class ButtonAgent extends ReactiveAgent implements Observer{
    private final Msg msg;
    private final ReactiveAgent dest;
    
    public ButtonAgent(final Msg msg, final ReactiveAgent dest, ObservableButton btn){
        this.msg = msg;
        this.dest = dest;
        btn.addObserver(this);
    }
    @Override
    protected void processEvent(Event ev) {
        if(ev instanceof ButtonPressed) {
            this.sendMsgTo(dest, this.msg);
        }
    }

}
