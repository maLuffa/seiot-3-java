package seiot.model.agent;

import java.io.IOException;
import java.util.Calendar;

import seiot.model.core.AbstractState;
import seiot.model.core.Event;
import seiot.model.core.ReactiveAgent;
import seiot.model.device.Light;
import seiot.model.event.TimerDelta;
import seiot.model.log.Logger;
import seiot.model.msg.Msg;
import seiot.model.msg.MsgButton;
import seiot.model.msg.MsgEvent;
import seiot.model.msg.MsgManager;
import seiot.model.msg.MsgOutOfBound;
import seiot.model.msg.MsgRadar;
/**
 * the logic of system
 */
public class Manager extends ReactiveAgent{
	private static final String ERR_LED = "ERRORE LED!!";
	private final static int DIST_MIN = 20;
	private final static int ANGLE_MIN = 0;
	private final static int ANGLE_MAX = 180;
	private final static TimerDelta DT = new TimerDelta(100);
	private Event currentEvent;
	private final Light on;
	private final Blinker detected;
	private final Light tracking;
	private ReactiveAgent agent;
	private seiot.model.core.State currentState;
	private int obstacle;
	public Manager(final Light onLed, 
			final Blinker detectedLed, final Light tracking) {
		this.on = onLed;
		this.detected = detectedLed;
		this.tracking = tracking;
		this.currentState = new Off();
		this.obstacle = 0;
	}
	public void setDestination(ReactiveAgent radar) {
		this.agent = radar;
	}
	@Override
	protected void processEvent(Event ev) {
		this.currentEvent = ev;
		this.currentState.exec();
		this.currentState = this.currentState.nextState();
	}

	private class Work extends AbstractState{
		@Override
		public void exec() {
			if(currentEvent instanceof MsgEvent) {
				Msg msg = ((MsgEvent) currentEvent).getMsg();
				if(msg instanceof MsgButton.Off) {
					Manager.this.sendMsgTo(agent, new MsgManager.TurnOff());
					switchLed(false, on, tracking);
					super._nextState = new Off();
				} else if(msg instanceof MsgRadar) {
					obstacle ++;
					double distance = ((MsgRadar) msg).getDistance();
					int angle = (((MsgRadar) msg).getAngle()).intValue();
					//TRACKING
					if(distance <= DIST_MIN) {
						logging(" - Object tracking at angle " + angle +" distance " + distance);
						_nextState = new Tracking();
						switchLed(true, tracking);

						Manager.this.sendMsgTo(agent, new MsgManager.Stop());
					//DETECTED
					} else {
						logging(" - Object detected at angle " + angle);
						detected.notifyEvent(DT);
					}
					if(angle == ANGLE_MIN || angle == ANGLE_MAX) {
						Logger.instance().log("TOT OBSTACLE = " + obstacle);
						obstacle = 0;
					}
				} else if(msg instanceof MsgOutOfBound) {
					
					int angle = (int)(((MsgOutOfBound) msg).getAngle());
					if(angle == ANGLE_MIN || angle == ANGLE_MAX) {
						Logger.instance().log("TOT OBSTACLE = " + obstacle);
						obstacle = 0;
					}
				}
			}
		}
	}
	private class Off extends AbstractState{
		@Override
		public void exec() {
			if(currentEvent instanceof MsgEvent) {
				if(((MsgEvent)currentEvent).getMsg() instanceof MsgButton.On) {
					Manager.this.sendMsgTo(agent, new MsgManager.TurnOn());
					switchLed(true, on);
					super._nextState = new Work();
				}
			}
		}
	}
	private class Tracking extends AbstractState{
		@Override
		public void exec() {
			if(currentEvent instanceof MsgEvent) {
				Msg msg = ((MsgEvent) currentEvent).getMsg();
				if(msg instanceof MsgButton.Off) {
					Manager.this.sendMsgTo(agent, new MsgManager.TurnOff());
					switchLed(false, on, tracking);
					super._nextState = new Off();
				} else if(msg instanceof MsgOutOfBound) {
					switchLed(false,tracking);
					super._nextState = new Work();
					Manager.this.sendMsgTo(agent, new MsgManager.Restart());
				} else if(msg instanceof MsgRadar){
					if(((MsgRadar) msg).getDistance() > DIST_MIN) {
						switchLed(false,tracking);
						super._nextState = new Work();
						Manager.this.sendMsgTo(agent, new MsgManager.Restart());
					}
				}
			}
		}
	}
	private void logging(final String value) {
		final Calendar cal = Calendar.getInstance();
		Logger.instance().log("Time " + cal.get(Calendar.HOUR)+
				":"+cal.get(Calendar.MINUTE)+
				":"+cal.get(Calendar.SECOND)+
				value);
	}
	private void switchLed(final boolean state,Light ... lights) {
		try {
			for(int i = 0; i < lights.length; i++) {
				if(state) lights[i].switchOn(); else lights[i].switchOff();
			}
		} catch (IOException e) {
			Logger.instance().log(ERR_LED);
		}
	}
}
