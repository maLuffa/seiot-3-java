package seiot.model.agent;

import java.util.Optional;

import seiot.model.core.Event;
import seiot.model.core.Observer;
import seiot.model.core.ReactiveAgent;
import seiot.model.device.ObservableRadar;
import seiot.model.event.AngleEvent;
import seiot.model.event.DistanceEvent;
import seiot.model.msg.MinMsg;
import seiot.model.msg.Msg;
import seiot.model.msg.MsgEvent;
import seiot.model.msg.MsgOutOfBound;
import seiot.model.msg.MsgManager.Restart;
import seiot.model.msg.MsgManager.Stop;
import seiot.model.msg.MsgManager.TurnOff;
import seiot.model.msg.MsgManager.TurnOn;
import seiot.model.msg.MsgRadar;
import seiot.model.msg.PlusMsg;
/**
 * an agent used to control a radar
 */
public class RadarAgent extends ReactiveAgent implements Observer{
	private final Manager dest;
    private final double maxDist;
    private Optional<Double> lastDistance;
    private final ObservableRadar radar;
    public RadarAgent(final Manager dest, final ObservableRadar radar) {
    	this.dest = dest;
    	this.maxDist = radar.getMaxDist();
    	this.radar = radar;
    	radar.addObserver(this);
    	lastDistance = Optional.empty();
	}
	@Override
	protected void processEvent(Event ev) {
		if(ev instanceof DistanceEvent) {
			double distance = ((DistanceEvent) ev).getDistance();
			if(distance > maxDist) {
				this.lastDistance = Optional.empty();
			} else {
				this.lastDistance = Optional.of(distance);
			}
		} else if(ev instanceof AngleEvent) {
			Msg msg = null;
			if(this.lastDistance.isPresent()) {
				msg = new MsgRadar(this.lastDistance.get(), ((AngleEvent) ev).getAngle());
			} else {
				msg = new MsgOutOfBound(((AngleEvent) ev).getAngle());
			}
			this.sendMsgTo(dest, msg);
		} if(ev instanceof MsgEvent) {
			Msg msg = ((MsgEvent) ev).getMsg();
			if(msg instanceof TurnOn) {
				radar.on();
			} else if(msg instanceof TurnOff) {
				radar.off();
			} else if(msg instanceof Stop) {
				radar.stop();
			} else if(msg instanceof Restart) {
				radar.restart();
			} else if(msg instanceof PlusMsg) {
				radar.increaseVelocity();
			} else if(msg instanceof MinMsg) {
				radar.decreaseVelocity();
			}
		}
	}

}
